define([
	'require',
	'madjoh_modules/cache/data_cache',
	'madjoh_modules/json_url/json_url'
],
function(require, DataCache, JSON_URL){
	var Language = {
		primary : 'en',

		get : function(){
			// NAVIGATOR
				var lang = navigator.language || navigator.userLanguage;
				lang = lang.split('-')[0];

			// CACHE (USER PREFERENCE)
				var cachedLanguage = DataCache.get('language');
				if(cachedLanguage) lang = cachedLanguage;

			// URL
				var urlLang = JSON_URL.toJSON(document.location.href).lang;
				if(urlLang) lang = urlLang;

			// DEFAULT
				if(!lang) lang = Language.primary;

			return lang;
		},
		getDefault : function(){
			return Language.primary;
		}
	};

	return Language;
});