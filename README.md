# language #

v1.0.3

This module's purpose is to provide the user's favorite language.

For now it provides the browser language.

### Language codes : ###

([source](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes))

| Code 	| Language 		| 
| ------| ------------- |
| fr	| French		|
| en	| English		|

